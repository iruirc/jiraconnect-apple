//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import UIKit

class NewFeedbackViewController: UIViewController {
  var sendFeebackAction: SendFeedbackAction?
  var settings: FeedbackSettings? // TODO: Settings should not be optional.
  
  var rootView: NewFeedbackView {
    return view as! NewFeedbackView
  }
  var screenshotImage: UIImage? = nil

  var onDidFinish: (Void -> Void)?

  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = "newFeedbackFlow_navigationTitle".localizedString
    navigationItem.rightBarButtonItem = createSendBarButtonItem()
    settings?.getReporterInfoAsynchronously? { usernameOrEmail, avatarURL in
      dispatch_async(dispatch_get_main_queue()) {
        if let usernameOrEmail = usernameOrEmail {
            self.settings?.reporterUsernameOrEmail = usernameOrEmail
        }
        if let avatarURL = avatarURL {
          self.rootView.composeFeedbackView.avatarImageURL = avatarURL
        }
      }
    }
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    populateViewWithInitialData()
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    rootView.presentDeleteScreenshotButton()
  }
  
  func populateViewWithInitialData() {
    rootView.screenshotImageOrNil = screenshotImage  // TODO: Make decision to use orNil or not to use and standardize.
    if let avatarImage = settings?.reporterAvatarImage {
      rootView.avatarImage = UIImage(CGImage: avatarImage)
    }
  }
  
  // TODO: Consider moving cancel action here since there is no flow.
  func cancel() {
    rootView.dismissKeyboard()
  }
  
  func sendFeedback() {
    let feedback = rootView.feedbackText
    let issue = Issue(feedback: feedback, components: settings!.issueComponents, type: settings!.issueType,
      reporterUsernameOrEmail: settings!.reporterUsernameOrEmail)
    let action = SendFeedbackAction(issue: issue, screenshotImageOrNil: rootView.screenshotImageOrNil) { outcome in
      switch outcome {
      case .Success:
        self.rootView.showSuccessMessage()
        delayOnMainQueue(1) {
          self.onDidFinish?()
        }
        
      case.Error:
        self.rootView.showErrorMessage()
        delayOnMainQueue(2) {
          self.enableNavigationItems()
          self.rootView.transitionFromSendingBackToEditing()
        }
        
      case .Cancelled:
        break // TODO: Support cancellation in UI and in here.
      }
    }
    action.start()
    sendFeebackAction = action
    
    disableNavigationItems()
    rootView.transitionToSending()
  }
  
  @IBAction func deleteScreenshot(sender: AnyObject) {
    let alertControllerStyle: UIAlertControllerStyle = .Alert
    let confirmationAlertController = UIAlertController(
      title: "",
      message: "newFeedback_deleteScreenshotConfirmation_message".localizedString,
      preferredStyle: alertControllerStyle)
    let deleteAction = UIAlertAction( titleKey: "newFeedback_deleteScreenshot_actionTitle".localizedString, style: .Destructive) { action in
      // TODO: Animate layout change here? If text view content is long, the compose view will overtake the screen, 
      //         but it snaps into place instead of animating.
      self.rootView.animateDeleteScreenshot()
      
    }
    confirmationAlertController.addAction(deleteAction)
    confirmationAlertController.addCancelAction()
    presentViewController(confirmationAlertController, animated: true, completion: nil)
  }
  
  func enableNavigationItems() {
    self.navigationItem.leftBarButtonItem?.enabled = true
    self.navigationItem.rightBarButtonItem?.enabled = true
  }
  
  func disableNavigationItems() {
    navigationItem.leftBarButtonItem?.enabled = false
    navigationItem.rightBarButtonItem?.enabled = false
  }
  
  func presentKeyboard() {
    rootView.presentKeyboard()
  }
  
  override func willTransitionToTraitCollection(newCollection: UITraitCollection, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
    super.willTransitionToTraitCollection(newCollection, withTransitionCoordinator: coordinator)
    rootView.willTransitionToTraitCollection(newCollection, withTransitionCoordinator: coordinator)
  }
  
  func createSendBarButtonItem() -> UIBarButtonItem {
    return UIBarButtonItem(title: "global_send_button_title".localizedString, style: .Done, target: self, action: #selector(NewFeedbackViewController.sendFeedback))
  }
}

// Move this out when needed by other files.
func delayOnMainQueue(delay:Double, closure:()->()) {
  dispatch_after(
    dispatch_time(
      DISPATCH_TIME_NOW,
      Int64(delay * Double(NSEC_PER_SEC))
    ),
    dispatch_get_main_queue(), closure)
}
