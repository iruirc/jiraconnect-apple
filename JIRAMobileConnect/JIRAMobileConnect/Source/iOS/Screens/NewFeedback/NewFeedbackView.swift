//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import UIKit

// TODO: Make all view properties private across module.
class NewFeedbackView: UIView {
  @IBOutlet private weak var sendingIndicator: SendingIndicatorView!
  @IBOutlet weak var screenshotPreviewView: ScreenshotPreviewView!
  @IBOutlet weak var composeFeedbackView: ComposeFeedbackView! // TODO: Make this private again
  
  @IBOutlet private weak var composeTextFieldBottomConstraint: NSLayoutConstraint! // TODO: Move this constraint into compose view.
  @IBOutlet private weak var screenshotTopMarginConstraint: NSLayoutConstraint!
  @IBOutlet private weak var screenshotBottomAnchor: NSLayoutConstraint!
  @IBOutlet private weak var composeViewBottomConstraint: NSLayoutConstraint!
  
  var transitioning = false
  
  override func awakeFromNib() {
    super.awakeFromNib()
    hideSendingIndicator()
    applyInitialStyle()
    addDismissKeyboardGestureRecognizers()
    
    // TODO: Place in method and have VC turn this on and off.
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(NewFeedbackView.handleKeyboardWillShowNotification(_:)), name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(NewFeedbackView.handleKeyboardWillChangeFrameNotification(_:)), name: UIKeyboardWillChangeFrameNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(NewFeedbackView.handleKeyboardKeyboardWillHideNotification(_:)), name: UIKeyboardWillHideNotification, object: nil)
  }
  
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }
  
  func applyInitialStyle() {
    let backgroundColor = ADGChromeColor
    self.backgroundColor = backgroundColor
    sendingIndicator.backgroundColor = backgroundColor
    screenshotPreviewView.backgroundColor = backgroundColor
  }
  
  func addDismissKeyboardGestureRecognizers() {
    let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(NewFeedbackView.dismissKeyboard))
    screenshotPreviewView.addGestureRecognizer(tapGestureRecognizer)
    
    let swipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(NewFeedbackView.dismissKeyboard))
    swipeGestureRecognizer.direction = [.Down, .Up]
    screenshotPreviewView.addGestureRecognizer(swipeGestureRecognizer)
  }
  
  override func layoutSubviews() {
    screenshotPreviewView.updateScreenshotImageViewFixedWidthUsingMaxWidth(frame.width)
    super.layoutSubviews()
  }
  
  func updateComposeFeedbackViewBottomContraintConstant(constant: CGFloat) {
    composeViewBottomConstraint.constant = constant
  }
  
  func insertSubviewBelowComposeFeedbackView(subview subview:UIView) {
    insertSubview(subview, belowSubview: composeFeedbackView)
  }
  
  func animateDeleteScreenshot() {
    screenshotPreviewView.animateDeleteScreenshot {
      // TODO: Look for retain cycles source wide.
      self.removeScreenshotPreviewView()
    }
  }
  
  func removeScreenshotPreviewView() {
    // NOTE: Support adding screenshot after feedback flow is presented.
    screenshotPreviewView.removeFromSuperview()
    composeFeedbackView.dismissKeyboardButton.hidden = true
  }
  
}

// MARK: States

extension NewFeedbackView {
  
  func transitionToSending() {
    dontPutTextFieldFlushAgainstBottomOfComposeViewIfNeeded()
    showSendingIndicator()
    disableScreenshotDelete()
    makeRoomForSendingIndicator()
    dismissKeyboard()
  }
  
  func transitionFromSendingBackToEditing() {
    moveUpScreenshotAsResultOfNoSendingIndicator()
    hideSendingIndicator()
    putTextFieldFlushAgainstBottomOfComposeViewIfNeeded()
    enableScreenshotDelete()
  }
  
}

// MARK: Layout

extension NewFeedbackView {
  
  func moveUpScreenshotAsResultOfNoSendingIndicator() { // TODO: Use shared constant for 60.
    screenshotTopMarginConstraint.constant -= 60
  }
  
  func makeRoomForSendingIndicator() {
    screenshotTopMarginConstraint.constant += 60
  }
  
  // TODO: Figure out what this and the next method is doing, probably if compose view moves down when sending don't need the text field to collapse because compose
  //   view is probably crushed vertically instead of retaining height and being moved down.
  func dontPutTextFieldFlushAgainstBottomOfComposeViewIfNeeded() {
    // TODO: Nil out screenshotContainerClippingView if user deletes screenshot so the image's memory can be released
    if let _ = screenshotPreviewView.superview {
      composeTextFieldBottomConstraint.active = false
    }
  }
  
  func putTextFieldFlushAgainstBottomOfComposeViewIfNeeded() {
    if let _ = screenshotPreviewView.screenshotContainerClippingView?.superview {
      composeTextFieldBottomConstraint.active = true
    }
  }
  
}

// MARK: Indicator Methods

extension NewFeedbackView {
  
  func showSendingIndicator() {
    sendingIndicator.indicateSending()
    sendingIndicator.hidden = false
  }
  
  func hideSendingIndicator() {
    sendingIndicator.hidden = true
  }
  
  func showSuccessMessage() {
    sendingIndicator.indicateSuccess()
  }
  
  func showErrorMessage() {
    sendingIndicator.indicateError(errorMessage: "newFeedback_send_errorMessage".localizedString)
  }

  func presentAvatarIfNeededAnimated(animated: Bool, withDelay delay: NSTimeInterval) {
    if !animated {
      composeFeedbackView.avatarImageView?.alpha = 1
    } else {
      guard let avatarImageView = composeFeedbackView.avatarImageView else { return }
      guard avatarImageView.alpha == 0 else { return }
      avatarImageView.transform = CGAffineTransformScale(avatarImageView.transform, 0.1, 0.1)
      UIView.animateKeyframesWithDuration(
        0.1,
        delay: delay,
        options: [],
        animations: {
          avatarImageView.alpha = 1
        },
        completion: nil)
      UIView.animateWithDuration(
        0.6,
        delay: delay,
        usingSpringWithDamping: 0.5,
        initialSpringVelocity: 0.9,
        options: [],
        animations: {
          avatarImageView.transform = CGAffineTransformIdentity
        }, completion: nil)
    }
  }
  
}

// MARK: Screenshot Preview View passthrough

extension NewFeedbackView {
  
  var screenshotImageOrNil: UIImage? {
    get {
      return screenshotPreviewView.screenshotImageOrNil
    }
    set {
      screenshotPreviewView.screenshotImageOrNil = newValue
      if newValue == nil {
        removeScreenshotPreviewView()
      }
    }
  }
  
  var dimmingViewAlpha: CGFloat {
    get {
      return screenshotPreviewView.dimmingViewAlpha
    }
  }
  
  func disableScreenshotDelete() {
    screenshotPreviewView.disableScreenshotDelete()
  }
  
  func enableScreenshotDelete() {
    screenshotPreviewView.enableScreenshotDelete()
  }
  
  func updatePropertiesPostTransition() {
    screenshotPreviewView.updatePropertiesPostTransition()
  }
  
  func uncroppedScreenshotImageViewFrame() -> CGRect {
    return screenshotPreviewView.uncroppedScreenshotImageViewFrame()
  }
  
  func hideScreenshot() {
    screenshotPreviewView.hideScreenshot()
  }
  
  func showScreenshot() {
    screenshotPreviewView.showScreenshot()
  }
  
  // TODO: Standardize on show/hide/present/animate method names.
  func presentDeleteScreenshotButton() { // TODO: Is this something the VC needs to know about? Maybe add a hook that VC tells view it did appear and do what you need.
    screenshotPreviewView.presentDeleteScreenshotButton()
  }
  
}

// MARK: Compose Feedback View passthrough

extension NewFeedbackView {
  
  var feedbackText: String {
    get {
      return composeFeedbackView.feedbackText
    }
  }
  
  var avatarImage: UIImage? {
    get {
      return composeFeedbackView.avatarImage
    }
    set {
      composeFeedbackView.avatarImage = newValue
    }
  }
  
  func presentKeyboard() {
    composeFeedbackView.presentKeyboard()
  }
  
  func dismissKeyboard() {
    composeFeedbackView.dismissKeyboard()
  }
  
}

// MARK: Keyboard

extension NewFeedbackView {
  // Handles Screen Rotation
  
  // TODO: Clean this, it's from the VC, looks like views don't get this method.
  func willTransitionToTraitCollection(newCollection: UITraitCollection, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
    transitioning = true
    updateComposeFeedbackViewBottomContraintConstant(0)
    
    screenshotPreviewView.screenshotShadowView?.alpha = 0
    coordinator.animateAlongsideTransition(nil, completion: { context in
      self.screenshotPreviewView.screenshotShadowView?.alpha = 1
    })
    // TODO: also update feedback text view text container insets to align with avatar offset change in landscape/compact height
  }
  
  override func traitCollectionDidChange(previousTraitCollection: UITraitCollection?) {
    super.traitCollectionDidChange(previousTraitCollection)
    transitioning = false
  }
  
  
  // Handles Keyboard appearance and frame changes
  
  // TODO: Handle keyboard height changes and keyboard dismiss.
  func handleKeyboardWillShowNotification(notification: NSNotification) {
    guard let userInfo = notification.userInfo else { return }
    
    let keyboardAnimationDuration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
    let animationCurve = UIViewAnimationOptions(rawValue: (userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber)?.unsignedIntegerValue ?? UInt(0))
    
    UIView.animateWithDuration(keyboardAnimationDuration,
      delay: 0,
      options: animationCurve,
      animations: { () -> Void in
        //        self.rootView.dimmingView.alpha = 1.0
        self.composeFeedbackView.dismissKeyboardButton.alpha = 1
      },
      completion: nil)

    presentAvatarIfNeededAnimated(true, withDelay: keyboardAnimationDuration)
  }
  
  func handleKeyboardWillChangeFrameNotification(notification: NSNotification) {
    guard let userInfo = notification.userInfo else { return }
    
    let keyboardAnimationDuration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
    let keyboardEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.CGRectValue() ?? CGRectZero
    let animationCurve = UIViewAnimationOptions(rawValue: (userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber)?.unsignedIntegerValue ?? UInt(0))
    
    if !transitioning {
      if let window = window {
        let bottomMargin = max(window.frame.size.height - keyboardEndFrame.origin.y, 0)
        // This must go before setting bottom constraint, or else breaks constraints.
        if bottomMargin > 0 && screenshotPreviewView.superview != nil {
          screenshotBottomAnchor.active = false
        }
        updateComposeFeedbackViewBottomContraintConstant(bottomMargin)
        // This must go after setting bottom constraint, or else breaks constraints.
        if bottomMargin <= 0 && screenshotPreviewView.superview != nil {
          screenshotBottomAnchor.active = true
        }
      }
    }
    
    UIView.animateWithDuration(keyboardAnimationDuration,
      delay: 0,
      options: animationCurve,
      animations: { () -> Void in
        self.layoutIfNeeded()
      },
      completion: nil)
  }
  
  func handleKeyboardKeyboardWillHideNotification(notification: NSNotification) {
    guard let userInfo = notification.userInfo else { return }
    
    let keyboardAnimationDuration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
    let animationCurve = UIViewAnimationOptions(rawValue: (userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber)?.unsignedIntegerValue ?? UInt(0))
    
    UIView.animateWithDuration(keyboardAnimationDuration,
      delay: 0,
      options: animationCurve,
      animations: { () -> Void in
        //        self.rootView.dimmingView.alpha = 0.0
        self.composeFeedbackView.dismissKeyboardButton.alpha = 0
      },
      completion: nil)
  }
}
