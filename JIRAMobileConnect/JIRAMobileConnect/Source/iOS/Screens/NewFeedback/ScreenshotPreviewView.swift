//  Copyright © 2016 Atlassian Pty Ltd. All rights reserved.

import UIKit

// This class is only meant to be used by Interface Builder.
class ScreenshotPreviewView: UIView {
  // All subview properties are optional because they may be removed from the view hierarchy and will get nilled out.
  //  Using optionals instead of implicitly unwrapped optionals makes sure any nilled out subview is not accidentally unwrapped.
  @IBOutlet weak var screenshotLabel: UILabel?
  @IBOutlet weak var screenshotImageView: ScreenshotImageView? // TODO: Make this private again
  @IBOutlet weak var dimmingView: UIView? // TODO: Make this private again
  @IBOutlet weak var screenshotContainerClippingView: UIView?
  @IBOutlet weak var screenshotShadowView: UIView?
  @IBOutlet weak var deleteScreenshotButton: UIButton?
  
  let dimmingViewAlpha: CGFloat = 0.2
  var screenshotImageOrNil: UIImage? {
    didSet {
      if let imageView = screenshotImageView {
        imageView.image = screenshotImageOrNil
      }
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    applyStyle()
    screenshotImageView?.image = screenshotImageOrNil
  }

  func applyStyle() {
    clipsToBounds = false
    
    screenshotLabel?.textColor = ADGGray
    
    screenshotImageView?.superview?.clipsToBounds = true
    screenshotImageView?.superview?.layer.cornerRadius = 8
    
    screenshotShadowView?.backgroundColor = UIColor.clearColor()
    screenshotShadowView?.layer.shadowColor = UIColor.blackColor().CGColor
    screenshotShadowView?.layer.shadowOpacity = 0.4
    screenshotShadowView?.layer.shadowOffset = CGSize(width: 0, height: 1.5)
    screenshotShadowView?.layer.shadowRadius = 1.8
    
    dimmingView?.alpha = dimmingViewAlpha
    dimmingView?.backgroundColor = UIColor.blackColor()
  }
  
  func animateDeleteScreenshot(onComplete onComplete: (Void -> Void)? = nil) {
    screenshotShadowView?.removeFromSuperview()
    deleteScreenshotButton?.removeFromSuperview()
    UIView.animateWithDuration(
      0.3,
      animations: {
        self.screenshotContainerClippingView?.scale(0.1, 0.1)
        self.screenshotContainerClippingView?.alpha = 0
        self.screenshotLabel?.alpha = 0
      },
      completion: { completed in
        self.screenshotContainerClippingView?.removeFromSuperview()
        self.screenshotImageView?.image = nil
        self.screenshotLabel?.removeFromSuperview()
        onComplete?()
    })
  }
  
  func disableScreenshotDelete() {
    deleteScreenshotButton?.hidden = true
  }
  
  func enableScreenshotDelete() {
    deleteScreenshotButton?.hidden = false
  }
  
  func hideScreenshot() {
    screenshotImageView?.hidden = true
  }
  
  func showScreenshot() {
    screenshotImageView?.hidden = false
  }
  
  func updatePropertiesPostTransition() {
    screenshotImageView?.hidden = false
  }
  
  func presentDeleteScreenshotButton() {
    UIView.animateWithDuration(0.3) {
      self.deleteScreenshotButton?.alpha = 1.0
    }
  }
}

// MARK: Layout

extension ScreenshotPreviewView {
  
  // TODO: fix all references to superview!
  override func layoutSubviews() {
    super.layoutSubviews()
    updateShadowPathIfNeeded()
  }
  
  func updateShadowPathIfNeeded() {
    if let screenshotShadowView = screenshotShadowView, screenshotContainerClippingView = screenshotContainerClippingView {
      screenshotShadowView.layer.shadowPath = UIBezierPath(roundedRect: screenshotContainerClippingView.bounds, cornerRadius: 8).CGPath
    }
  }
  
  func updateScreenshotImageViewFixedWidthUsingMaxWidth(maxWidth: CGFloat) {
    let horizontalMargin: CGFloat = 12
    let rawWidth = screenshotImageView?.originalIntrinsicContentSize().width
    if rawWidth > maxWidth - horizontalMargin * 2 {
      screenshotImageView?.fixedWidth = maxWidth - horizontalMargin * 2
    } else {
      screenshotImageView?.fixedWidth = nil
    }
  }
  
  func uncroppedScreenshotImageViewFrame() -> CGRect {
    guard let screenshotImageView = screenshotImageView else {
      return CGRectZero
    }
    var frame = CGRectZero
    frame.origin = screenshotImageView.superview!.superview!.frame.origin // TODO: Clean this math up.
    frame.size = screenshotImageView.frame.size
    return frame
  }
}

// NOTE: Move this extension out once another file needs this logic.
extension UIView {
  private func scale(sx: CGFloat, _ sy: CGFloat) {
    self.transform = CGAffineTransformScale(self.transform, sx, sy)
  }
}
