//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import UIKit

class HuggingContainerView: UIView {
  var viewToHug: UIView! {
    willSet {
      viewToHug?.removeFromSuperview()
    }
    didSet {
      addSubview(viewToHug)
    }
  }
  
  override func layoutSubviews() {
    viewToHug?.frame = bounds
    super.layoutSubviews()
  }
}
