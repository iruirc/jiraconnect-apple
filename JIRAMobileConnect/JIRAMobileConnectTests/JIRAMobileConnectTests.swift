//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import JIRAMobileConnect

import XCTest
@testable import JIRAMobileConnect

class JIRAMobileConnectTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        
        
        let exp = expectationWithDescription("exp")

        
        let target = JMCTarget(host: "rcachatx.atlassian.net", apiKey: "af1f4aa5-46d9-4da2-a238-e6f2affdace4", projectKey: "HCVQ1")
        let issue = Issue(summary: "Test", description: "Test", components: ["iOS"], type: "Bug")
        
        let action = SendFeedbackAction(issue: issue, target: target) { outcome in
            switch outcome {
            case .Success:
                print("Success")
            case .Error:
                print("Error")
            case .Cancelled:
                print("Cancelled")
            }
            exp.fulfill()
        }
        
        action.start()
        waitForExpectationsWithTimeout(10, handler: nil)
        
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
