Pod::Spec.new do |s|
  s.name         = "JIRAMobileConnect"
  s.version      = "2.0.0-alpha.9"
  s.summary      = "Enables JIRA to collect user feedback."
  s.description  = <<-DESC
                   JIRA Mobile Connect enables JIRA to collect user feedback for your mobile apps. Key features include:

                   * In-app User feedback- Get feedback from your mobile users or testers
                   DESC

  s.homepage     = "https://bitbucket.org/atlassian/jiraconnect-apple"
  s.license      = "Apache License, Version 2.0"
  s.authors      = { "René Cacheaux" => "rcacheaux@atlassian.com"  }
  s.platform     = :ios, "8.0"
  s.source       = { :git => "https://bitbucket.org/atlassian/jiraconnect-apple.git", :tag => s.version.to_s }
  s.source_files = 'JIRAMobileConnect/JIRAMobileConnect/**/*.{swift}'
  s.resources = "JIRAMobileConnect/JIRAMobileConnect/**/*.{png,jpeg,jpg,storyboard,xib,xcassets,lproj}"
  s.requires_arc = true
  s.dependency 'Alamofire', '~> 2.0'
  s.dependency 'SDWebImage', '~> 3.7.3'
end
